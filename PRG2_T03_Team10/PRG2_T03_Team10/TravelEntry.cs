﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }

        public string EntryMode { get; set; }

        public DateTime EntryDate { get; set; }

        public DateTime ShnEndDate { get; set; }

        public SHNFacility ShnStay { get; set; }

        public bool IsPaid { get; set; }

        public TravelEntry() { }

        public TravelEntry(string l, string em, DateTime ed) 
        {
            LastCountryOfEmbarkation = l;
            EntryMode = em;
            EntryDate = ed;
        }

        public void AssignSHNFacility(SHNFacility s)
        {
            ShnStay = s;
        }

        public DateTime CalculateSHNDuration()
        {
            if(LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                return ShnEndDate = EntryDate;
                
            }
            else if(LastCountryOfEmbarkation == "Macao SAR")
            {
                return ShnEndDate = EntryDate.AddDays(7);
            }
            else
            {
                return ShnEndDate = EntryDate.AddDays(14);
            }         
        }

        public override string ToString()
        {
            return  "LastCountryOfEmbarkation: " + LastCountryOfEmbarkation + "\n" +
                    "Entry Mode: " + EntryMode + "\n" +
                    "Entry Date: " + EntryDate + "\n" +
                    "Stay Home Notice End Date: " + ShnEndDate;
        }

       
    }
}
