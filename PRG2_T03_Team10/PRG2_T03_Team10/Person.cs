﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    abstract class Person
    {
        public string Name { get; set; }

        public List<SafeEntry> SafeEntryList { get; set; }

        public List<TravelEntry> TravelEntryList { get; set; }

        public Person() { }

        public Person(string nam) 
        {
            Name = nam;
            SafeEntryList = new List<SafeEntry>();
            TravelEntryList  = new List<TravelEntry>();
        }

        public void AddTravelEntry(TravelEntry t)
        {
            TravelEntryList.Add(t);
        }

        public void AddSafeEntry(SafeEntry s)
        {
            SafeEntryList.Add(s);
        }      

        public abstract double CalculateSHNCharges();
        
        public override string ToString()
        {
            return Name;
        }


    }
}
