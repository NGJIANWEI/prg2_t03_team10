﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class TraceTogetherToken
    {
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        public TraceTogetherToken() { }

        public TraceTogetherToken(string ser, string col, DateTime exp)
        {
            SerialNo = ser;
            CollectionLocation = col;
            ExpiryDate = exp;
        }

        //Checks if the current DateTime is past 1 month before a token's expiry, if it is returns true, else return false
        public bool EligibleForReplacement()
        {
            if (DateTime.Now > ExpiryDate.AddMonths(-1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Replace token by assigning new datetime, serial and collection location
        public void ReplaceToken(string serial, string colloc)
        {
            DateTime generatedExpiry = DateTime.Now.AddMonths(6);
            SerialNo = serial;
            CollectionLocation = colloc;
            ExpiryDate = generatedExpiry;
        }

        public override string ToString()
        {
            return "Serial Number: " + SerialNo + "\n" +
                "Collection Location: " + CollectionLocation + "\n" +
                "Expiry Date: " + ExpiryDate;
        }

    }
}
