﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        public Visitor(string nam,string pas, string nat):base(nam)
        {
            PassportNo = pas;
            Nationality = nat;           
        }

        //Calculating SHNCharges for Person
        public override double CalculateSHNCharges()
        {
            //Setting values
            double swabTest = 200;
            double transportation = 80;
            double sdfCharges = 2000;
            double gst = 1.07;
            double shnCharges = 0;

            TravelEntry travelEntryObjects = TravelEntryList[TravelEntryList.Count - 1];

            //If there is an existing SHN in effect, add SHN costs into charges
            if (travelEntryObjects.ShnStay != null)
            {
                shnCharges = (swabTest + travelEntryObjects.ShnStay.CalculateTravelCost(travelEntryObjects.EntryMode, travelEntryObjects.EntryDate) + sdfCharges) * gst; //if Shn Facility assigned, add the travelCost
            }
            //Else only charge them for transport and swab test
            else
            {
                shnCharges = (swabTest + transportation) * gst;
            }
            return shnCharges;
        }

        public override string ToString()
        {
            return base.ToString()+"\t\t"+PassportNo+"\t\t"+Nationality; 
        }
    }
}
