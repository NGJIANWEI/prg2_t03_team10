﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class BusinessLocation
    {
        public string businessName { get; set; }

        public string branchCode { get; set; }

        public int maximumCapacity { get; set; }

        public int visitorsNow { get; set; }

        public BusinessLocation() { }

        public BusinessLocation(string bn, string bc, int mc)
        {
            businessName = bn;
            branchCode = bc;
            maximumCapacity = mc;
        }

        public bool IsFull()
        {
            if (visitorsNow == maximumCapacity)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Business Name: " + businessName + "\n" +
                "Branch Code: " + branchCode + "\n" +
                "Visitors Now: " + visitorsNow + "\n" +
                "Maximum capacity: " + maximumCapacity + "\n";
        }
    }
}
