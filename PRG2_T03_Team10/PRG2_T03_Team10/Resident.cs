﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class Resident : Person
    {
        public string Address { get; set; }

        public DateTime LastLeftCountry { get; set;}

        public TraceTogetherToken Token { get; set; }

        public Resident(string nam, string add, DateTime las) : base(nam)
        {
            Address = add;
            LastLeftCountry = las;
        }

        public override double CalculateSHNCharges()
        {
            double swabTest = 200;
            double sdfCharge = 1000;
            double transportation = 20;
            double gst = 1.07;

            TravelEntry travelEntryObjects = TravelEntryList[TravelEntryList.Count - 1];

            if (travelEntryObjects.LastCountryOfEmbarkation == "New Zealand" || travelEntryObjects.LastCountryOfEmbarkation == "Vietnam")
            {
                return swabTest * gst; //For New Zealand and Vietnam, only swab test fee
            }
            else if (travelEntryObjects.LastCountryOfEmbarkation == "Macao SAR")
            {
                return (swabTest + transportation) * gst; // For Macao Sar, there will be a swab test and transportation fee
            }
            else
            {
                return (sdfCharge + swabTest + transportation) * gst; // For all other countries, there will be a swab test, transportation fee and SHN dedicated facility fee
            }
        }

        public override string ToString()
        {
            return base.ToString() + Address + "\t" + LastLeftCountry;
        }

    }
}
