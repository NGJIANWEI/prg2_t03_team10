﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class SafeEntry
    {
        public DateTime CheckIn { get; set; }

        public DateTime CheckOut { get; set; }

        public BusinessLocation Location { get; set; }

        public SafeEntry() { }

        public SafeEntry(DateTime ci, BusinessLocation l)  /*checkOut will be set when person checks out*/
        {
            CheckIn = ci;
            Location = l;
        }

        //Setting checkout value when Person checks out from location, decreases visitorsNow in location to reflect number of visitors
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
            Location.visitorsNow -= 1;
        }

        public override string ToString()
        {
            if (Convert.ToString(CheckOut) != "1/1/0001 12:00:00 AM")
            {
                return "Check-in time: " + CheckIn + "\n" +
                "Check-out time: " + CheckOut + "\n" +
                "Location: " + Location.businessName + "\n" +
                "Branch code: " + Location.branchCode + "\n" +
                "Visitors now: " + Location.visitorsNow + "\n" +
                "Maximum capacity: " + Location.maximumCapacity;
            }
            else
            {
                return "Check-in time: " + CheckIn + "\n" +
                "Check-out time: " + "" + "\n" +
                "Location: " + Location.businessName + "\n" +
                "Branch code: " + Location.branchCode + "\n" +
                "Visitors now: " + Location.visitorsNow + "\n" +
                "Maximum capacity: " + Location.maximumCapacity;
            }
        }
    }
}
