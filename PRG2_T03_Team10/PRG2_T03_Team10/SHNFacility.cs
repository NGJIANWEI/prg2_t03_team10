﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T03_Team10
{
    class SHNFacility
    {
        public string FacilityName { get; set; }

        public int FacilityCapacity { get; set; }

        public int FacilityVacancy { get; set; }

        public double DistFromAirCheckpoint { get; set; }

        public double DistFromSeaCheckpoint { get; set; }

        public double DistFromLandCheckpoint { get; set; }

        public SHNFacility() { }

        public SHNFacility(string fName, int fCapacity, double dAir, double dSea, double dLand) 
        {
            FacilityName = fName;
            FacilityCapacity = fCapacity;
            FacilityVacancy = fCapacity;
            DistFromAirCheckpoint = dAir;
            DistFromSeaCheckpoint = dSea;
            DistFromLandCheckpoint = dLand;
        }

        public double CalculateTravelCost(string em, DateTime ed)
        {
            double baseCharge = 0;
            if(em == "Land")
            {
                baseCharge = 50 + DistFromLandCheckpoint * 0.22;            
            }
            else if (em == "Air")
            {
                baseCharge = 50 + DistFromAirCheckpoint * 0.22;
            }
            else if (em == "Sea")
            {
                baseCharge = 50 + DistFromSeaCheckpoint * 0.22;
            }

            if ((ed.Hour >= 6 && ed.Hour < 9) || (ed.Hour > 18 && ed.Hour < 24))
            {
                baseCharge = baseCharge * 1.25;
            }

            else if ((ed.Hour >= 0 && ed.Hour < 6))
            {
                baseCharge = baseCharge * 1.5;
            }
            return baseCharge;
        }

        public bool IsAvailable()
        {
            if(FacilityVacancy > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\n" +
                "Capacity: " + FacilityCapacity + "\n" +
                "Vacancy: " + FacilityVacancy + "\n" +
                "Distance From Air Checkpoint: " + DistFromAirCheckpoint + "\n" +
                "Distance From Sea Checkpoint: " + DistFromSeaCheckpoint + "\n" +
                "Distance From Land Checkpoint: " + DistFromLandCheckpoint + "\n";
        }

    }

}
