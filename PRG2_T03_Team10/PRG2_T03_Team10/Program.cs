﻿//============================================================
// Student Number : S10208269, S10194239
// Student Name : Ng Jian Wei, Ozzy Tham
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;


namespace PRG2_T03_Team10
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
              ===========================================================================
              ===================== L O A D    C L A S S    L I S T =====================
              ===========================================================================
            **/
            List<SHNFacility> facilityList = new List<SHNFacility>();
            List<Person> personList = new List<Person>();
            List<BusinessLocation> businessList = new List<BusinessLocation>();

            /**
              ====================================================================================
              ===================== L O A D    S H N F A C I L I T Y   A P I =====================
              ====================================================================================
            **/
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    facilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                    foreach (SHNFacility s in facilityList)
                    {
                        s.FacilityVacancy = s.FacilityCapacity;
                    }
                }
            }

            /**
             ============================================================
             ===================== L O A D    C S V =====================
             ============================================================
            **/
            LoadBusiness(businessList);
            LoadPerson(personList, facilityList);

            do
            {
                displayMenu();
                string userInput = Console.ReadLine();

                if (userInput == "1")
                {
                    DisplayVisitorList(personList);
                }
                else if (userInput == "2")
                {
                    DisplayPersonDetails(personList);
                }
                else if (userInput == "3")
                {
                    AssignReplaceTraceTogether(personList);
                }
                else if (userInput == "4")
                {
                    DisplayBusinessList(businessList);
                }
                else if (userInput == "5")
                {
                    EditBusinessCapacity(businessList);
                }
                else if (userInput == "6")
                {
                    SafeEntryCheckIn(personList, businessList);
                }
                else if (userInput == "7")
                {
                    SafeEntryCheckOut(personList, businessList);
                }
                else if (userInput == "8")
                {
                    LoadFacility(facilityList);
                }
                else if (userInput == "9")
                {
                    CreateVisitor(personList);
                }
                else if (userInput == "10")
                {
                    CreateTravelEntry(personList, facilityList);
                }
                else if (userInput == "11")
                {
                    CalculateSHNCharges(personList);
                }
                else if (userInput == "12")
                {
                    ContactTracingReporting(personList, businessList);
                }
                else if (userInput == "13")
                {
                    SHNStatusReport(personList);
                }
                else if (userInput == "0")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Invalid input, please try again.");
                }
            } while (true);
        }
        /**
            ==========================================================================
            ======================== D I S P L A Y    M E N U ========================
            ==========================================================================
        **/
        static void displayMenu()
        {
            Console.Write("\n//===================================================================================\n" +
                "//================= C O V I D    M O N I T O R I N G    S Y S T E M =================\n" +
                "//===================================================================================\n\n" +
                "[1] List all visitors\n" +
                "[2] List all person details\n" +
                "[3] Assign/Replace TraceTogether Token\n" +
                "[4] List all Business Locations\n" +
                "[5] Edit Business Location Capacity\n" +
                "[6] SafeEntry Check-In\n" +
                "[7] SafeEntry Check-Out\n" +
                "[8] List all SHN Facilities\n" +
                "[9] Create Visitor\n" +
                "[10] Create TravelEntry Record\n" +
                "[11] Calculate SHN Charges\n" +
                "[12] Contact Tracing Reporting\n" +
                "[13] SHN Status Reporting\n" +
                "[0] Exit\n\n" +
                "---------------------------------------------\n" +
                "Enter your option: ");
        }
        static void LoadPerson(List<Person> personList, List<SHNFacility> facilityList)
        {

            string[] csvLines = File.ReadAllLines("Person.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                //CSV values are assigned to variables
                string[] lines = csvLines[i].Split(',');
                string type = lines[0];
                string name = lines[1];
                string address = lines[2];
                var lastLeftCountry = lines[3];
                string passportNo = lines[4];
                string nationality = lines[5];
                string tokenSerial = lines[6];
                string tokenCollectionLocation = lines[7];
                var tokenExpiryDate = lines[8];
                string travelEntryLastCountry = lines[9];
                string travelEntryMode = lines[10];
                var travelEntryDate = lines[11];
                var travelShnEndDate = lines[12];
                string facilityName = lines[14];

                TravelEntry travelEntry = null;
                if (travelEntryLastCountry != "" || travelEntryMode != "" || travelShnEndDate != "") /*if travelEntry columns in CSV is not empty, create a travelEntry object with the relevant information*/
                {
                    travelEntry = new TravelEntry(travelEntryLastCountry, travelEntryMode, Convert.ToDateTime(travelEntryDate));
                }

                if (type == "visitor") //If first column is visitor, create a visitor object with relevant information 
                {
                    Person visitor = new Visitor(name, passportNo, nationality);
                    personList.Add(visitor);
                    if (travelEntryLastCountry != "")
                    {
                        visitor.AddTravelEntry(travelEntry);
                        visitor.TravelEntryList[0].IsPaid = Convert.ToBoolean(lines[13]);
                    }
                    if (travelShnEndDate != "") //If SHN end date is not empty, take it in and add it into object
                    {
                        visitor.TravelEntryList[0].ShnEndDate = Convert.ToDateTime(travelShnEndDate);
                    }
                }
                else // else if person is a resident
                {
                    //If type of Person is not visitor, it is Resident
                    Person person = new Resident(name, address, Convert.ToDateTime(lastLeftCountry));
                    Resident resident = (Resident)person;
                    //If there is no expiry date, set it to null
                    if (tokenExpiryDate == "")
                    {
                        tokenExpiryDate = null;
                    }
                    //Create and a new token with relevant values from the CSV, even if empty
                    resident.Token = new TraceTogetherToken(tokenSerial, tokenCollectionLocation, Convert.ToDateTime(tokenExpiryDate));
                    personList.Add(resident);
                    //If travelEntryLastCountry is not empty, they have travelled recently
                    if (travelEntryLastCountry != "")
                    {
                        //Add the travelEntrry object since they travelled recently
                        resident.AddTravelEntry(travelEntry);
                        resident.TravelEntryList[0].IsPaid = Convert.ToBoolean(lines[13]);
                    }
                    if (travelShnEndDate != "")//If SHN end date is not empty, take it in and add it into object
                    {
                        resident.TravelEntryList[0].ShnEndDate = Convert.ToDateTime(travelShnEndDate);
                    }
                }

                SHNFacility shnFacility = null;
                if (facilityName != "") // or null
                {
                    foreach (SHNFacility s in facilityList) //search SHNFacility list
                    {
                        if (s.FacilityName == facilityName) //if person.csv facility name matches the facility name in SHNFacility class
                        {
                            shnFacility = s;
                            s.FacilityVacancy = s.FacilityVacancy - 1; // minus 1 vacancy each time a person is loaded into the facility
                            break;
                        }
                    }

                    if (shnFacility != null)
                    {
                        travelEntry.AssignSHNFacility(shnFacility);
                    }
                }

            }
        }

        /**
              ===========================================================================
              =================== L O A D    B U S I N E S S    C S V ===================
              ===========================================================================
        **/
        static void LoadBusiness(List<BusinessLocation> businessList)
        {
            string[] csvLines = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] lines = csvLines[i].Split(',');
                string businessName = lines[0];
                string branchCode = lines[1];
                int maximumCapacity = Convert.ToInt32(lines[2]);
                BusinessLocation businessLocation = new BusinessLocation(businessName, branchCode, maximumCapacity);
                businessList.Add(businessLocation);
            }
        }

        /**
             =================================================================================
             =================== D I S P L A Y    V I S I T O R    L I S T ===================
             =================================================================================
       **/
        static void DisplayVisitorList(List<Person> personList)
        {
            Console.WriteLine("\n//==============================================================\n" +
                                      "//======================= V I S I T O R ========================\n" +
                                      "//==============================================================\n");
            Console.WriteLine("{0}\t\t{1}\t\t{2}\t\t\n" +
                "-----           ----------------        ------------"
                , "Name", "Passport Number", "Nationality");

            foreach (Person p in personList) 
            {
                if (p is Visitor) //If the person in list is a visitor, display the ToString() value taken from visitor.cs
                {
                    Console.WriteLine(p.ToString()); 
                }
            }
        }

        /**
             =====================================================================================
             =================== D I S P L A Y    P E R S O N    D E T A I L S ===================
             =====================================================================================
       **/
        static void DisplayPersonDetails(List<Person> personList)
        {
            Console.WriteLine("\n//===============================================================\n" +
                                      "//================= P E R S O N    D E T A I L S ================\n" +
                                      "//===============================================================\n");
            Console.Write("Enter name of the person: ");

            string userInput = Console.ReadLine();

            bool found = false;
            foreach (Person p in personList)
            {
                //If person's name matches user's input,
                if (userInput.ToLower() == p.Name.ToLower())
                {
                    Console.WriteLine("\n========= TravelEntry Details =========\n");
                    found = true;
                    try
                    {
                        //Try to print TravelEntryList details
                        Console.WriteLine("{0}",p.TravelEntryList[0].ToString());
                    }
                    catch
                    {
                        //If TravelEntryList is empty, print this
                        Console.WriteLine("No TravelEntry details are found for {0}.", p.Name);
                    }

                    Console.WriteLine("\n========= SafeEntry Details =========\n");
                    try
                    {
                        //Try to print SafeEntryList details
                        if (p.SafeEntryList.Count > 1)
                        {
                            for (int x = 0; x < p.SafeEntryList.Count; x++) //1 person can have multiple safe entry record, so using loop to display all safe entry list
                            {
                                Console.WriteLine("{0}\n", p.SafeEntryList[x].ToString());
                            }
                        }
                        //If SafeEntryList is empty, print this
                        else
                        {
                            Console.WriteLine("{0}\n", p.SafeEntryList[0].ToString());
                        }                  
                    }
                    catch
                    {
                        //If TravelEntryList is empty, print this
                        Console.WriteLine("No SafeEntry details are found for {0}.", p.Name);
                    }

                    //If the person found is a resident,
                    if (p is Resident)
                    {
                        //Print the TraceTogetherToken
                        Console.WriteLine("\n========= TraceTogetherToken Details =========\n");
                        //Cast the Person item into resident item to access token
                        Resident r = (Resident)p;
                        //If token is empty, tell user there is no token
                        if (r.Token.SerialNo == "")
                        {
                            Console.WriteLine("This person does not have a TraceTogether Token.");
                        }
                        //Print token details if not empty
                        else
                        {
                            Console.WriteLine(r.Token.ToString());
                        }
                    }
                    break;
                }
            }
            if (found == false)
            {
                Console.WriteLine("No such person found in our COVID Monitoring System.");
            }
        }

        /**
             =============================================================================================================
             =================== A S S I G N / R E P L A C E    T R A C E T O G E T H E R    T O K E N ===================
             =============================================================================================================
       **/
        static void AssignReplaceTraceTogether(List<Person> pList)
        {
            Console.Write("\n//=========================================================================================================\n" +
                "//================= A S S I G N / R E P L A C E    T R A C E T O G E T H E R    T O K E N =================\n" +
                "//=========================================================================================================\n\n");
            Random rnd = new Random();
            Console.Write("Enter name: ");
            string userName = Console.ReadLine();
            bool foundName = false;     /*if by end of foreach loop it doesn't find the user's input name, foundName remains false and program gives an error message and runs this method again*/
            foreach (Person personObj in pList)
            {
                if (personObj.Name.ToLower() == userName.ToLower())
                {
                    foundName = true;           /* Input is found and foundName is set to true*/
                    //If the personObj that is found that matches user's input name is a resident
                    if (personObj is Resident)
                    {
                        Resident residentObj = (Resident)personObj;
                        //There is no token item in personObj
                        if (residentObj.Token.SerialNo == "")
                        {
                            string generatedSerial = "T" + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10);         /*Creates new serial number*/
                            string generatedCollectionLocation = "TBD CC";         /* Creates new collection location*/
                            residentObj.Token.ReplaceToken(generatedSerial, generatedCollectionLocation);
                            Console.WriteLine("New token is assigned.");
                            Console.WriteLine("\n");
                            Console.Write(residentObj.Token.ToString());
                            Console.WriteLine("\n");
                        }
                        //There is token but it is eligible for replacement
                        else if (residentObj.Token.EligibleForReplacement() == true)
                        {
                            string generatedSerial = "T" + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10) + rnd.Next(0, 10);         /*Creates new serial number*/
                            string generatedCollectionLocation = "TBD CC";          /* Creates new collection location*/
                            residentObj.Token.ReplaceToken(generatedSerial, generatedCollectionLocation);
                            Console.WriteLine("Token has been replaced.");
                            Console.WriteLine("\n");
                            Console.Write(residentObj.Token.ToString());
                            Console.WriteLine("\n");
                        }
                        //There is token but it is not eligible for replacement
                        else
                        {
                            Console.WriteLine("Existing TraceTogether token is not eligible for replacement.");
                            Console.WriteLine("\n");
                        }
                    }
                    //Person is not a resident, and as a result is not eligible for a trace together token
                    else
                    {
                        Console.WriteLine("Person is not eligible to receive TraceTogether token.");
                        Console.WriteLine("\n");
                    }
                }
            }
            if (foundName == false)
            {
                Console.WriteLine("Enter a valid name."); /*if foundName is not set to true because there were no matching names, recall method and inform user*/
            }
        }


        /**
             ===================================================================================
             =================== D I S P L A Y    B U S I N E S S    L I S T ===================
             ===================================================================================
       **/
        static void DisplayBusinessList(List<BusinessLocation> bList)
        {
            Console.Write("\n//==============================================================\n" +
                "//================= B U S I N E S S    L I S T =================\n" +
                "//==============================================================\n\n");
            foreach (BusinessLocation item in bList)
            {
                Console.WriteLine(item.ToString());
            }
        }

        /**
             ===================================================================================
             =================== E D I T    B U S I N E S S    C A P A C I T Y =================
             ===================================================================================
       **/
        static void EditBusinessCapacity(List<BusinessLocation> bList)
        {
            Console.Write("Enter business location branch code: ");
            string userBranch = Console.ReadLine();
            bool foundBranch = false; /*if by end of foreach loop it doesn't find the user's input name, foundBranch remains false and program gives an error message and runs this method again*/
            foreach (BusinessLocation item in bList)
            {
                if (userBranch == item.branchCode)
                {
                    Console.Write("Enter new maximum capacity: ");
                    int newMax = Convert.ToInt32(Console.ReadLine());
                    item.maximumCapacity = newMax; /*gets and sets a new maximum capacity for the businessLocation*/
                    foundBranch = true; /*Sets to true since it was found*/
                    Console.WriteLine("New maximum capacity has been set!");
                    break;
                }
            }
            if (foundBranch == false)
            {
                Console.WriteLine("Invalid branch code, please try again."); /*if foundBranch is not set to true because there were no matching names, recall method and inform user*/
            }
        }

        /**
              ============================================================================================================
              =================================== S A F E E N T R Y    C H E C K - I N ===================================
              ============================================================================================================
        **/
        static void SafeEntryCheckIn(List<Person> pList, List<BusinessLocation> bList)
        {
            Console.Write("Enter your name: ");
            string userName = Console.ReadLine();
            bool foundName = false;     /*if by end of foreach loop it doesn't find the user's input name, foundName remains false and program gives an error message and runs this method again*/
            foreach (Person p in pList)
            {
                if (userName.ToLower() == p.Name.ToLower())
                {
                    foundName = true;  /* Input is found and foundName is set to true*/
                    DisplayBusinessList(bList);
                    Console.Write("Enter business location: ");
                    string userBLInput = Console.ReadLine();
                    bool foundBusiness = false;         /*if by end of foreach loop it doesn't find the user's input name, foundBusiness remains false and program gives an error message and runs this method again*/
                    foreach (BusinessLocation bl in bList)
                    {
                        if (userBLInput.ToLower() == bl.businessName.ToLower() || userBLInput.ToLower() == bl.branchCode.ToLower()) /*User can input either branch code or business name to select from the list*/
                        {
                            if (bl.IsFull() == false)       /*if current visitors don't match maximum capacity add visitors and safeentry object to person's list*/
                            {
                                bl.visitorsNow += 1;
                                SafeEntry newSafeEntryPerson = new SafeEntry(DateTime.Now, bl);
                                p.AddSafeEntry(newSafeEntryPerson);
                                Console.WriteLine("Successfully used SafeEntry Check-In.");
                                foundBusiness = true;       /* Input is found and foundName is set to true*/
                            }
                            else
                            {
                                Console.WriteLine("Business location has reached maximum capacity.");
                                foundBusiness = true;   /* Input is found and foundName is set to true*/
                            }
                        }
                    }
                    if (foundBusiness == false)
                    {
                        Console.WriteLine("Invalid business name or branch code, please try again.");
                    }
                }
            }
            if (foundName == false)
            {
                Console.WriteLine("No such person found in our COVID Monitoring System.");
            }

        }

        /**
              ============================================================================================================
              =================================== S A F E E N T R Y    C H E C K - O U T ===================================
              ============================================================================================================
        **/
        static void SafeEntryCheckOut(List<Person> pList, List<BusinessLocation> bList)
        {
            Console.Write("Enter your name: ");
            string userName = Console.ReadLine();
            bool foundName = false;         /*if by end of foreach loop it doesn't find the user's input name, foundName remains false and program gives an error message and runs this method again*/
            bool checkOutAttempt = false;
            foreach (Person p in pList)
            {
                if (userName.ToLower() == p.Name.ToLower())
                {
                    //If there is a person with a matching name as the user input
                    foundName = true;
                    //If the person's safe entry list has no objects, end the method
                    if (p.SafeEntryList.Count < 1)
                    {
                        Console.WriteLine("No SafeEntry records found.");
                    }
                    else
                    {
                        Console.WriteLine("\n========= SafeEntry Details =========\n");
                        //Displaying all records
                        for (int u = 0; u < p.SafeEntryList.Count; u++)
                        {
                            Console.WriteLine("{0}\n",p.SafeEntryList[u].ToString());
                        }
                        //Get user input, either Business' name or Branch code
                        Console.Write("Enter business name or branch code to check-out from: ");
                        string userRecord = Console.ReadLine();
                        for (int i = 0; i < p.SafeEntryList.Count; i++)
                        {
                            //If input matches
                            if (userRecord.ToLower() == p.SafeEntryList[i].Location.businessName.ToLower() || userRecord == p.SafeEntryList[i].Location.branchCode.ToLower())
                            {
                                //If CheckOut datetime is not set to default value, person already checked out
                                if (Convert.ToString(p.SafeEntryList[i].CheckOut) != "1/1/0001 12:00:00 AM")
                                {
                                    Console.WriteLine("Already checked out of business location.");
                                    checkOutAttempt = true;
                                }
                                //Check out of business else
                                else
                                {
                                    p.SafeEntryList[i].PerformCheckOut();
                                    Console.WriteLine("Checked out successfully.");
                                    checkOutAttempt = true;
                                }
                            }
                            //If cannot find matching, inform user
                            
                        }
                    }
                }
            }
            if (foundName == false)
            {
                Console.WriteLine("No such person found in our COVID Monitoring System.");               
            }
            else if(checkOutAttempt == false)
            {
                Console.WriteLine("Person does not have a safe entry record under the specified business name or branch code, please try again.");
            }
        }

        /**
             ===================================================================================
             =================== L O A D    F A C I L I T Y    L I S T ===================
             ===================================================================================
       **/
        static void LoadFacility(List<SHNFacility> facilityList)
        {
            Console.WriteLine("\n//==============================================================\n" +
                                      "//====================== F A C I L I T Y =======================\n" +
                                      "//==============================================================\n");
            Console.WriteLine("\n========= List of facilities =========\n");
            foreach (SHNFacility list in facilityList)
            {
                Console.WriteLine(list.ToString());
            }
        }

        /**
             ====================================================================
             =================== C R E A T E    V I S I T O R ===================
             ====================================================================
       **/
        static void CreateVisitor(List<Person> personList)
        {
            Console.WriteLine("\n//===========================================================================\n" +
                                      "//====================== C R E A T E    V I S I T O R =======================\n" +
                                      "//===========================================================================\n");

            bool found = false;
            //Get user input for name of new visitor
            Console.Write("Enter visitor name: ");
            string nameInput = Console.ReadLine();
            //Check if person already exists
            for (int i = 0; i < personList.Count; i++)
            {
                if (nameInput.ToLower() == personList[i].Name.ToLower())
                {
                    found = true;
                    Console.WriteLine("Person already exist.");
                    break;
                }
            }

            if (found == false) //If person is not found
            {
                //Get user input for relevant details needed for visitor object
                Console.Write("Enter visitor passport Number: ");
                string passportInput = Console.ReadLine();

                Console.Write("Enter visitor nationality: ");
                string nationalityInput = Console.ReadLine();

                //Create the visitor object and add into the list
                Person visitorList = new Visitor(nameInput, passportInput, nationalityInput);
                personList.Add(visitorList);

                Console.WriteLine("Visitor Successfully Created!");
            }
        }

        /**
             ===============================================================================
             =================== C R E A T E    T R A V E L    E N T R Y ===================
             ===============================================================================
       **/
        static void CreateTravelEntry(List<Person> personList, List<SHNFacility> facilityList) //Creating a new travel entry object into person
        {
            Console.Write("\n//===========================================================================\n" +
                "//================= C R E A T E    T R A V E L    E N T R Y =================\n" +
                "//===========================================================================\n\n");

            Console.Write("Enter name: "); //Get user input for name of new visitor
            string nameInput = Console.ReadLine();
            TravelEntry travelEntry = null;
            bool found = false;

            //Check if visitor already has a travel entry object
            for (int i = 0; i < personList.Count; i++) 
            { 
                if (nameInput.ToLower() == personList[i].Name.ToLower())
                {
                    found = true;
                    try
                    {
                        if (personList[i].TravelEntryList[0].LastCountryOfEmbarkation != null) //Check if there is an existing travel entry object, if there is, end method
                        {
                            Console.WriteLine("This person already has travelEntry record.");
                        }
                    }
                    //Else get relevant details for creating travel entry object and add to list
                    catch
                    {
                        Console.Write("Enter last country of embarkation: ");
                        string lastCountryEmbarkInput = Console.ReadLine();
                        lastCountryEmbarkInput = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(lastCountryEmbarkInput.ToLower()); //convert all first letter to uppercase

                        if (string.IsNullOrWhiteSpace(lastCountryEmbarkInput))
                        {
                            Console.WriteLine("Please input a country.");
                            break;
                        }

                        Console.Write("Enter select entry mode: \n" +
                            "[1] Air Checkpoint\n" +
                            "[2] Sea Checkpoint\n" +
                            "[3] Land Checkpoint\n" +
                            "Your option: ");
                        string entryModeInput = Console.ReadLine();
                        
                        if (entryModeInput == "1")
                        {
                            entryModeInput = "Air";
                        }
                        else if (entryModeInput == "2")
                        {
                            entryModeInput = "Sea";
                        }
                        else if (entryModeInput == "3")
                        {
                            entryModeInput = "Land";
                        }
                        else
                        {
                            Console.WriteLine("Invalid entry mode");
                            break;
                        }
                        //Create the travel entry object and add it to list, calculate the SHN Duration based on the new info
                        try
                        {
                            Console.Write("Enter entry date (dd/MM/yyyy HH:mm:ss): ");
                            DateTime entryDateInput = Convert.ToDateTime(Console.ReadLine());
                            travelEntry = new TravelEntry(lastCountryEmbarkInput, entryModeInput, Convert.ToDateTime(entryDateInput));
                            personList[i].AddTravelEntry(travelEntry);
                            Console.WriteLine("Stay Home Notice End Date: {0}", personList[i].TravelEntryList[0].CalculateSHNDuration());
                            Console.WriteLine("Successfully Added.");
                        }
                        catch
                        {
                            Console.WriteLine("Invalid date time.");
                            break;
                        }
                        
                        if (lastCountryEmbarkInput != "new zealand" || lastCountryEmbarkInput != "vietnam" || lastCountryEmbarkInput != "macao sar") //If country are not those specified above
                        {
                            Console.WriteLine("Please select a facility.");
                            for (int j = 0; j < facilityList.Count; j++)
                            {
                                Console.WriteLine("[{0}] {1}", (j + 1), facilityList[j].FacilityName);
                            }
                            //Prompt user to choose a facility
                            Console.Write("Your option: ");
                            int facilityInput = 0;
                            try
                            {
                                facilityInput = Convert.ToInt32(Console.ReadLine());
                            }
                            catch
                            {
                                Console.WriteLine("Wrong input, please try again.");
                            }
                            //If user input is more than 0 but less than or equals to the amount of facilities available,
                            if (facilityInput > 0 && facilityInput <= facilityList.Count)
                            {
                                foreach (SHNFacility shnFacility in facilityList)
                                {
                                    //If facility name is found in facilityList AND it is available
                                    if (facilityList[facilityInput - 1].IsAvailable() == true)
                                    {
                                        //Deduct vacancy by 1 and add the new shnFacility item to list
                                        shnFacility.FacilityVacancy = shnFacility.FacilityVacancy - 1;
                                        personList[i].TravelEntryList[0].AssignSHNFacility(shnFacility);
                                        Console.WriteLine("Facility assigned.");
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("No vacancy left.");
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid input, please try again.");
                            }
                        }
                        personList[i].AddTravelEntry(travelEntry);
                    }

                }
            }
            if (found == false)
            {
                Console.WriteLine("No such person found in our COVID Monitoring System.");
            }
        }

        /**
             ===================================================================================
             =================== C A L C U L A T E    S H N    C H A R G E S ===================
             ===================================================================================
       **/
        static void CalculateSHNCharges(List<Person> personList)
        {
            Console.Write("\n//===============================================================================\n" +
                "//================= C A L C U L A T E    S H N    C H A R G E S =================\n" +
                "//===============================================================================\n\n");
            bool found = false;
            Console.Write("Enter name: ");
            string nameInput = Console.ReadLine(); //Get user's name

            for (int i = 0; i < personList.Count; i++) //Find the name in the personList
            {
                if (nameInput.ToLower() == personList[i].Name.ToLower())
                {
                    found = true; //If person's name matches the user's input
                    try
                    {
                        if (personList[i].TravelEntryList[0].ShnEndDate < DateTime.Now && personList[i].TravelEntryList[0].IsPaid == false) //If SHN is over for user AND they haven't paid
                        {
                            Console.WriteLine("SHN Charges: {0}", personList[i].CalculateSHNCharges()); //Show user's outstanding payment and ask if they would like to pay
                            Console.Write("Would you like to make payment? [Y/N]: ");
                            string userInput = Console.ReadLine();
                            if (userInput.ToLower() == "yes" || userInput.ToLower() == "y") //If they would, set IsPaid to true
                            {
                                personList[i].TravelEntryList[0].IsPaid = true;
                                Console.WriteLine("Paid!");
                            }
                            else 
                            {
                                Console.WriteLine("You did not make payment.");
                                break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("No payment is due for {0}.", nameInput);
                        }
                    }
                    catch
                    {
                        Console.WriteLine("No payment is due for {0}.", nameInput);
                    }
                }
            }

            if (found == false)
            {
                Console.WriteLine("No such person found in our Covid Monitoring System.");
            }
        }

        /**
              ====================================================================================================
              ================= G E N E R A T E    C O N T A C T    T R A C I N G    R E P O R T =================
              ====================================================================================================
        **/
        static void ContactTracingReporting(List<Person> pList, List<BusinessLocation> bList)
        {
            Console.Write("\n//====================================================================================================\n" +
                "//================= G E N E R A T E    C O N T A C T    T R A C I N G    R E P O R T =================\n" +
                "//====================================================================================================\n\n");
            Console.Write("Enter date (DD/MM/YYYY): ");
            try
            {
                DateTime userDateTime = DateTime.Parse(Console.ReadLine());
                DisplayBusinessList(bList);
                Console.Write("Enter a business: ");
                string userBLInput = Console.ReadLine();
                bool businessCheck = false;
                foreach (Person p in pList)
                {
                    for (int i = 0; i < p.SafeEntryList.Count; i++)
                    {
                        if (p.SafeEntryList[i].Location.businessName == userBLInput || p.SafeEntryList[i].Location.branchCode == userBLInput)
                        {
                            businessCheck = true;
                            if (userDateTime < p.SafeEntryList[i].CheckIn && userDateTime.AddDays(1) > p.SafeEntryList[i].CheckIn)
                            {
                                using (StreamWriter sw = new StreamWriter($"{p.SafeEntryList[i].Location.businessName}.csv", false))
                                {
                                    sw.WriteLine("{0},{1},{2},{3},{4}", "checkIn", "checkOut", "businessName", "branchCode", "maximumCapacity");
                                    if (Convert.ToString(p.SafeEntryList[i].CheckOut) == "1/1/0001 12:00:00 AM")
                                    {
                                        sw.WriteLine("{0},{1},{2},{3},{4}", Convert.ToString(p.SafeEntryList[i].CheckIn), "NULL", Convert.ToString(p.SafeEntryList[i].Location.businessName), Convert.ToString(p.SafeEntryList[i].Location.branchCode), Convert.ToString(p.SafeEntryList[i].Location.maximumCapacity));
                                    }
                                    else
                                    {
                                        sw.WriteLine("{0},{1},{2},{3},{4}", Convert.ToString(p.SafeEntryList[i].CheckIn), Convert.ToString(p.SafeEntryList[i].CheckOut), Convert.ToString(p.SafeEntryList[i].Location.businessName), Convert.ToString(p.SafeEntryList[i].Location.branchCode), Convert.ToString(p.SafeEntryList[i].Location.maximumCapacity));
                                    }
                                    sw.Close();
                                }
                            }
                        }
                    }
                }
                if (businessCheck == false)
                {
                    Console.WriteLine("Invalid business/No SafeEntry records in business, please try again.");
                }
                else
                {
                    Console.WriteLine("Report successfully generated!");
                }
            }
            catch
            {
                Console.WriteLine("Please enter a valid date time.");
            }
        }

        /**
             ==============================================================================================
             =================== G E N E R A T E    S H N    S T A T U S    R E P O R T ===================
             ==============================================================================================
       **/
        static void SHNStatusReport(List<Person> personList)
        {

            Console.Write("\n//==========================================================================================\n" +
              "//================= G E N E R A T E    S H N    S T A T U S    R E P O R T =================\n" +
              "//==========================================================================================\n\n");
            DateTime? dateInput = null; //save user dateInput into this variable
            try //using try, we can catch the error caused by the person entering an invalid date format
            {
                Console.Write("Please enter a date to view report of all travellers serving their SHN [dd/MM/yyyy]: ");
                dateInput = Convert.ToDateTime(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Invalid Date/time, please try again.");
            }

            Console.WriteLine("");


            bool reportGenerated = false;
            using (StreamWriter sw = new StreamWriter("SHNStatusReporting.csv", false)) //create a "SHNStatusReporting csv file, i use true because i want to append a new line rather than overwrite
            {
                sw.WriteLine("{0},{1},{2},{3}", "name", "travelEntryDate", "travelShnEndDate", "facilityName");
                foreach (Person p in personList)
                {
                    try
                    {
                        if (dateInput >= p.TravelEntryList[0].EntryDate && dateInput <= p.TravelEntryList[0].ShnEndDate) // if user input date between entry date and end date
                        {
                            sw.WriteLine("{0},{1},{2},{3}", p.Name, p.TravelEntryList[0].EntryDate, p.TravelEntryList[0].ShnEndDate, p.TravelEntryList[0].ShnStay.FacilityName); // write name, entrydaate, shnenddate, facility name to the file we just created
                            reportGenerated = true;
                        }                 
                    }
                    catch
                    {
                        continue; //while scanning through the person list, if person doesn't have a datetime, it'll ignore it and go to the next line
                    }

                }
                sw.Close();
            }

            if(reportGenerated == true)
            {
                Console.WriteLine("Report successfully generated.");
            }
            else
            {
                Console.WriteLine("No one has a payment due on this date.");
            }
        }
    }
}

            
        